part of 'services.dart';

class ProductServices {

  static CollectionReference productCollection =
      Firestore.instance.collection('product');

  static Future<List<Product>> getProduct(String query, int limit) async {
    QuerySnapshot snapshot;
    if (query.trim() != "") {
      snapshot = await productCollection
          .reference()
          .where("nameProduct", isGreaterThanOrEqualTo: query.toString())
          .limit(limit)
          .getDocuments();
    } else {
      snapshot = await productCollection.limit(limit).getDocuments();
    }

    var documents = snapshot.documents;

    return documents
        .map((e) => Product(
            nameProduct: e.data['nameProduct'],
            descriptionProduct: e.data['subtitle'],
            colorProduct: e.data['colorProduct'],
            sizeProduct: e.data['sizeProduct'],
            priceProduct: e.data['priceProduct'],
            imageProduct: e.data['imageProduct']))
        .toList();
  }

//  static Future<List<Product>> getNextProduct(String query) async {
//    QuerySnapshot snapshot;
//    if (query.trim() != "") {
//      snapshot = await productCollection
//          .reference()
//          .where("nameProduct", isGreaterThanOrEqualTo: query.toString())
//          .limit(limit)
//          .startAfterDocument(documentList[documentList.length - 1])
//          .getDocuments();
//    } else {
//      snapshot = await productCollection.limit(limit).getDocuments();
//    }
//
//    var documents = snapshot.documents;
//
//    documentList.addAll(documents);
//
//    print(documents.map((e)=> e.data));
//
//    return documents
//        .map((e) => Product(
//            nameProduct: e.data['nameProduct'],
//            descriptionProduct: e.data['subtitle'],
//            colorProduct: e.data['colorProduct'],
//            sizeProduct: e.data['sizeProduct'],
//            priceProduct: e.data['priceProduct'],
//            imageProduct: e.data['imageProduct']))
//        .toList();
//  }

  static Future<void> saveTransaction(
      CreateProductData productTransaction) async {
    await productCollection.document().setData({
      'nameProduct': productTransaction.nameProduct,
      'descriptionProduct': productTransaction.descriptionProduct,
      'priceProduct': productTransaction.priceProduct,
      'colorProduct': productTransaction.colorProduct,
      'sizeProduct': productTransaction.sizeProduct,
      'imageProduct': productTransaction.imageProduct,
    });
  }
}
