part of 'shared.dart';

const double defaultMargin = 20;

Color mainColor = Color(0xFF323232);
Color accentColor1 = Color(0xFF3E3E3E);
Color accentColor2 = Color(0xFFADADAD);

TextStyle blackTextFont = GoogleFonts.raleway()
    .copyWith(color: Colors.black, fontWeight: FontWeight.w500);
TextStyle whiteTextFont = GoogleFonts.raleway()
    .copyWith(color: Colors.white, fontWeight: FontWeight.w500);
TextStyle greyTextFont = GoogleFonts.raleway()
    .copyWith(color: accentColor2, fontWeight: FontWeight.w500);

TextStyle whiteNumberFont = GoogleFonts.openSans()
    .copyWith(color: Color(0xFF323232));
TextStyle yellowNumberFont = GoogleFonts.openSans()
    .copyWith(color: accentColor2);
