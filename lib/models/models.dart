import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'user.dart';
part 'registration_data.dart';
part 'product.dart';
part 'create_product.dart';