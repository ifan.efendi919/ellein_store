part of 'models.dart';

class CreateProductData {
  String nameProduct;
  String descriptionProduct;
  int priceProduct;
  List<dynamic> sizeProduct;
  List<dynamic> colorProduct;
  String imageProduct;

  CreateProductData(
      {this.nameProduct = "",
        this.descriptionProduct = "",
        this.priceProduct = 0,
        this.sizeProduct = const [],
        this.colorProduct = const [],
        this.imageProduct});
}