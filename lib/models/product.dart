part of 'models.dart';

class Product extends Equatable {
  final String nameProduct;
  final String descriptionProduct;
  final List<dynamic> colorProduct;
  final List<dynamic> sizeProduct;
  final int priceProduct;
  final String imageProduct;

  Product(
      {
      @required this.nameProduct,
      @required this.descriptionProduct,
      @required this.colorProduct,
      @required this.sizeProduct,
      @required this.priceProduct,
      this.imageProduct});

  @override
  List<Object> get props => [
        nameProduct,
        descriptionProduct,
        colorProduct,
        sizeProduct,
        priceProduct,
        imageProduct
      ];
}
