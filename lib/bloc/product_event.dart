part of 'product_bloc.dart';

abstract class ProductEvent extends Equatable {
  const ProductEvent();
}

class GetProduct extends ProductEvent {
  final String query;
  final int limit;

  GetProduct(this.query, this.limit);

  @override
  List<Object> get props => [query];
}

//class GetNextProduct extends ProductEvent {
//  final String query;
//
//  GetNextProduct(this.query);
//
//  @override
//  List<Object> get props => [query];
//}