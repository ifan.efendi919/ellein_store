import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:store/models/models.dart';
import 'package:store/services/services.dart';

part 'product_event.dart';
part 'product_state.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  @override
  ProductState get initialState => ProductInitial();

  @override
  Stream<ProductState> mapEventToState(
    ProductEvent event,
  ) async* {
    if (event is GetProduct) {
      List<Product> product = await ProductServices.getProduct(event.query, event.limit);
      yield ProductLoaded(
          product: product
              .toList());
    }
  }
}
