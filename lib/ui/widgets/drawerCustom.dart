part of 'widgets.dart';

class EndNavDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    FirebaseUser firebaseUser = Provider.of<FirebaseUser>(context);

    if(firebaseUser == null){
      return Drawer(
        child: SafeArea(
          bottom: false,
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              ListTile(
                leading: Icon(Icons.input),
                title: Text(
                  'Login',
                  style: blackTextFont.copyWith(fontSize: UIUtills().sizeFont(fontsize: 12)),
                ),
                onTap: () {
                  context.bloc<PageBloc>().add(GoToLoginPage());
                },
              )
            ],
          ),
        ),
      );
    } else {
      return Drawer(
        child: SafeArea(
          bottom: false,
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              ListTile(
                leading: Icon(Icons.add_shopping_cart),
                title: Text(
                  'Create Product',
                  style: blackTextFont.copyWith(fontSize: UIUtills().sizeFont(fontsize: 12)),
                ),
                onTap: () {
                  context.bloc<PageBloc>().add(GoToCreateProductPage(CreateProductData()));
                },
              ),
              ListTile(
                leading: Icon(Icons.settings),
                title: Text(
                  'Setting',
                  style: blackTextFont.copyWith(fontSize: UIUtills().sizeFont(fontsize: 12)),
                ),
                onTap: () {

                },
              ),
              Divider(
                thickness: 1,
              ),
              ListTile(
                title: Text(
                  'Logout',
                  style: blackTextFont.copyWith(fontSize: UIUtills().sizeFont(fontsize: 12)),
                ),
                onTap: () {
                  AuthServices.signOut();
                  context.bloc<PageBloc>().add(GoToLoginPage());
                },
              ),
            ],
          ),
        ),
      );
    }
  }
}
