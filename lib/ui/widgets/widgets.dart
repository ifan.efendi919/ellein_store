import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:store/bloc/blocs.dart';
import 'package:store/bloc/page_bloc.dart';
import 'package:store/models/models.dart';
import 'package:store/services/services.dart';
import 'package:store/shared/shared.dart';

part 'drawerCustom.dart';
part 'input_tags.dart';