part of 'pages.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    FirebaseUser firebaseUser = Provider.of<FirebaseUser>(context);

    if (firebaseUser != null) {
      context.bloc<UserBloc>().add(LoadUser(firebaseUser.uid));
    }
    return BlocBuilder<PageBloc, PageState>(
        builder: (_, pageState) => (pageState is OnLoginPage)
                ? LoginPage() : (pageState is OnRegistrationPage) ? SignUpPage(RegistrationData())
                : (pageState is OnCreateProductPage)
                    ? ProductCreatePage(pageState.createProductData)
                    : MainPage());
  }
}
