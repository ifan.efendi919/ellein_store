part of 'pages.dart';

class ProductPage extends StatefulWidget {
  final Key scaffoldKey;

  const ProductPage({Key key, this.scaffoldKey}) : super(key: key);

  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  ScrollController _controllerListPage;
  ScrollController _controllerListProduct;
  ScrollPhysics listPage;
  ScrollPhysics listProduct;
  PageController pageController;
  int indexTab;
  String filter="";
  int limitProduct = 8;

  @override
  void initState() {
    super.initState();

    _controllerListPage = ScrollController();
    _controllerListProduct = ScrollController();
    listPage = ClampingScrollPhysics();
    listProduct = NeverScrollableScrollPhysics();
    indexTab = 0;
    pageController = PageController(initialPage: indexTab);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: scaffoldKey,
      drawerEdgeDragWidth: 0,
      endDrawer: EndNavDrawer(),
      body: NotificationListener(
        onNotification: (v) {
          if (v is ScrollEndNotification) {
            if (_controllerListPage.offset >= _controllerListPage.position.maxScrollExtent &&
                !_controllerListPage.position.outOfRange) {
              limitProduct = limitProduct + 4;
              context.bloc<ProductBloc>().add(GetProduct(filter, limitProduct));
//      movieListBloc.fetchNextMovies();
            }
            print(limitProduct);
          }
          return null;
        },
        child: ListView(
          controller: _controllerListPage,
          shrinkWrap: true,
          physics: listPage,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                left: UIUtills().getWidth(width: defaultMargin),
                top: UIUtills().getHeight(height: 10.0),
                right: UIUtills().getWidth(width: defaultMargin),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  BlocBuilder<UserBloc, UserState>(builder: (_, userState) {
                    if (userState is UserLoaded) {
                      if (imageFileToUpload != null) {
                        uploadImage(imageFileToUpload).then((downloadURL) {
                          imageFileToUpload = null;
                          context
                              .bloc<UserBloc>()
                              .add(UpdateData(profileImage: downloadURL));
                        });
                      }

                      return InkWell(
                        onTap: () => scaffoldKey.currentState.openEndDrawer(),
                        child: Container(
                          height: UIUtills().getWidth(width: 45),
                          width: UIUtills().getWidth(width: 45),
                          decoration: BoxDecoration(
                              color: Colors.grey,
                              borderRadius: BorderRadius.circular(10.0)),
                          child: ClipRRect(
                              borderRadius: BorderRadius.circular(10.0),
                              child: (userState.user.profilePicture != "")
                                  ? Image.network(userState.user.profilePicture,
                                      fit: BoxFit.cover, loadingBuilder:
                                          (BuildContext context, Widget child,
                                              ImageChunkEvent loadingProgress) {
                                      if (loadingProgress == null) return child;
                                      return Center(
                                        child: SpinKitFadingCircle(
                                          color: mainColor,
                                          size: 30,
                                        ),
                                      );
                                    })
                                  : Image.asset('assets/user.png')),
                        ),
                      );
                    } else {
                      return InkWell(
                        onTap: () => scaffoldKey.currentState.openEndDrawer(),
                        child: Container(
                          height: UIUtills().getWidth(width: 45),
                          width: UIUtills().getWidth(width: 45),
                          decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: BorderRadius.circular(10.0)),
                          child: ClipRRect(
                              borderRadius: BorderRadius.circular(10.0),
                              child: Image.asset('assets/user.png')),
                        ),
                      );
                    }
                  })
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                left: UIUtills().getWidth(width: defaultMargin),
                top: UIUtills().getHeight(height: 20.0),
                right: UIUtills().getWidth(width: defaultMargin),
                bottom: UIUtills().getHeight(height: 10.0),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Our',
                    style: blackTextFont.copyWith(
                        fontSize: UIUtills().sizeFont(fontsize: 26)),
                  ),
                  Text(
                    'Products',
                    style: blackTextFont.copyWith(
                        fontSize: UIUtills().sizeFont(fontsize: 30),
                        fontWeight: FontWeight.w600),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: UIUtills().getHeight(height: 15.0),
            ),
            Container(
              margin: EdgeInsets.symmetric(
                horizontal: UIUtills().getWidth(width: defaultMargin),
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: UIUtills().getWidth(width: 20.0)),
                      child: TextField(
                        autofocus: false,
                        cursorColor: Color(0xff333333),
                        decoration: InputDecoration(
                          filled: true,
                          prefixIcon: Icon(Icons.search),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide:
                                  BorderSide(color: Colors.transparent)),
                          hoverColor: Color(0xffEBEBEB),
                          fillColor: Color(0xffEBEBEB),
                          focusColor: Color(0xffEBEBEB),
                          contentPadding: EdgeInsets.all(0),
                          hintText: 'Search Product',
                          hintStyle: blackTextFont.copyWith(
                            fontSize: UIUtills().sizeFont(fontsize: 12),
                            fontWeight: FontWeight.w400,
                          ),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide:
                                  BorderSide(color: Colors.transparent)),
                          disabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide:
                                  BorderSide(color: Colors.transparent)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide:
                                  BorderSide(color: Colors.transparent)),
                        ),
                        onChanged: (val) {
                          setState(() {
                            filter = val;
                            context.bloc<ProductBloc>().add(GetProduct(filter, limitProduct));
                          });
                        },
                      ),
                    ),
                  ),
                  Container(
                    height: UIUtills().getWidth(width: 35),
                    width: UIUtills().getWidth(width: 35),
                    decoration: BoxDecoration(
                        color: Color(0xffffffff),
                        borderRadius: BorderRadius.circular(10.0)),
                    child: Icon(
                      Icons.filter_list,
                      color: Color(0xff797979),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: UIUtills().getHeight(height: 30.0),
            ),
            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: UIUtills().getWidth(width: defaultMargin)),
              height: UIUtills().getHeight(height: 50.0),
              child: ListView(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        indexTab = 0;
                      });
                    },
                    child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: UIUtills().getWidth(width: 20.0),
                            vertical: UIUtills().getWidth(width: 10.0)),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                                color: (indexTab == 0)
                                    ? Colors.deepOrangeAccent
                                    : Colors.black45,
                                width: (indexTab == 0) ? 2.0 : 1.0),
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                color: (indexTab == 0)
                                    ? Colors.deepOrange.withOpacity(0.2)
                                    : Colors.grey.withOpacity(0.2),
                                spreadRadius: 0,
                                blurRadius: 1,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ]),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Image.asset('assets/icon-tshirt.png'),
                            SizedBox(
                              width: UIUtills().getWidth(width: 5.0),
                            ),
                            Text(
                              'Clothes',
                              style: blackTextFont.copyWith(
                                  fontSize: UIUtills().sizeFont(fontsize: 14),
                                  fontWeight: FontWeight.w600),
                            )
                          ],
                        )),
                  ),
                  SizedBox(
                    width: UIUtills().getWidth(width: 10.0),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: UIUtills().getHeight(height: 30.0),
            ),
            Container(
              padding: EdgeInsets.symmetric(
                  horizontal: UIUtills().getWidth(width: defaultMargin)),
              child: BlocBuilder<ProductBloc, ProductState>(
                builder: (_, productState) {
                  if (productState is ProductLoaded) {
                    List<Product> product = productState.product;
                    return GridView.builder(
                        controller: _controllerListProduct,
                        physics: listProduct,
                        shrinkWrap: true,
                        itemCount: product.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            childAspectRatio:
                                MediaQuery.of(context).size.width /
                                    (MediaQuery.of(context).size.height * 0.7)),
                        itemBuilder: (_, index) {
                          return Container(
                            height: UIUtills().getHeight(height: 300),
                            margin: EdgeInsets.symmetric(
                              horizontal: UIUtills().getWidth(width: 10.0),
                              vertical: UIUtills().getHeight(height: 10.0),
                            ),
                            padding: EdgeInsets.symmetric(
                                horizontal: UIUtills().getWidth(width: 10.0),
                                vertical: UIUtills().getHeight(height: 15.0)),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10.0),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.2),
                                    spreadRadius: 0,
                                    blurRadius: 1,
                                    offset: Offset(
                                        0, 3), // changes position of shadow
                                  ),
                                ]),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                (product[index].imageProduct == null ||
                                        product[index].imageProduct == "")
                                    ? Align(
                                        alignment: Alignment.center,
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(5.0),
                                          child: Image.asset(
                                            'assets/user.png',
                                            height: UIUtills()
                                                .getHeight(height: 150.0),
                                            width: UIUtills()
                                                .getWidth(width: 120.0),
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      )
                                    : Align(
                                        alignment: Alignment.center,
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(5.0),
                                          child: Image.network(
                                            product[index].imageProduct,
                                            height: UIUtills()
                                                .getHeight(height: 150.0),
                                            width: UIUtills()
                                                .getWidth(width: 120.0),
                                            fit: BoxFit.cover,
                                            loadingBuilder:
                                                (BuildContext context,
                                                    Widget child,
                                                    ImageChunkEvent
                                                        loadingProgress) {
                                              if (loadingProgress == null)
                                                return child;
                                              return Center(
                                                child: SpinKitFadingCircle(
                                                  color: mainColor,
                                                  size: 30,
                                                ),
                                              );
                                            },
                                          ),
                                        ),
                                      ),
                                SizedBox(
                                  height: UIUtills().getHeight(height: 10.0),
                                ),
                                Align(
                                  alignment: Alignment.center,
                                  child: Text(
                                    '${product[index].nameProduct}',
                                    style: blackTextFont.copyWith(
                                        fontSize:
                                            UIUtills().sizeFont(fontsize: 14),
                                        fontWeight: FontWeight.bold),
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                                SizedBox(
                                  height: UIUtills().getHeight(height: 10.0),
                                ),
                                Align(
                                  alignment: Alignment.center,
                                  child: Text(
                                    NumberFormat.currency(
                                            locale: "id_ID",
                                            decimalDigits: 0,
                                            symbol: "IDR ")
                                        .format(product[index].priceProduct),
                                    style: whiteNumberFont.copyWith(
                                        fontSize:
                                            UIUtills().sizeFont(fontsize: 14),
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            ),
                          );
                        });
                  } else {
                    return SpinKitFadingCircle(
                      color: mainColor,
                      size: 50,
                    );
                  }
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
