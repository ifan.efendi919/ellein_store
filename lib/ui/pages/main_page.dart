part of 'pages.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int bottomNavBarIndex;
  PageController pageController;

  @override
  void initState() {
    super.initState();

    bottomNavBarIndex = 0;
    pageController = PageController(initialPage: bottomNavBarIndex);
  }

  @override
  Widget build(BuildContext context) {
    context
        .bloc<ThemeBloc>()
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: accentColor1)));
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(),
          SafeArea(bottom: false, child: Container(color: Color(0xFFF6F7F9))),
          Container(
            margin: EdgeInsets.only(bottom: 100),
            child: PageView(
              physics: NeverScrollableScrollPhysics(),
              controller: pageController,
              onPageChanged: (index) {
                setState(() {
                  bottomNavBarIndex = index;
                });
              },
              children: <Widget>[
                ProductPage(),
                Center(
                    child: Text(
                  'Search',
                )),
                Center(
                    child: Text(
                  'Keranjang',
                )),
                Center(
                    child: Text(
                  'Favorite',
                )),
              ],
            ),
          ),
          createCustomBottomNavBar()
        ],
      ),
    );
  }

  Widget createCustomBottomNavBar() => Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          height: UIUtills().getHeight(height: 100),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20),
                topRight: Radius.circular(20),
              )),
          child: BottomNavigationBar(
            elevation: 10,
            backgroundColor: Colors.white,
            selectedItemColor: mainColor,
            unselectedItemColor: Color(0xFF797979),
            currentIndex: bottomNavBarIndex,
            showSelectedLabels: false,
            showUnselectedLabels: false,
            selectedFontSize: UIUtills().sizeFont(fontsize: 12),
            unselectedFontSize: UIUtills().sizeFont(fontsize: 12),
            type: BottomNavigationBarType.fixed,
            onTap: (index) {
              setState(() {
                bottomNavBarIndex = index;
                pageController.jumpToPage(index);
              });
            },
            items: [
              BottomNavigationBarItem(
                  title: Text(''),
                  icon: (bottomNavBarIndex == 0)
                      ? Container(
                          padding:
                              EdgeInsets.all(UIUtills().getWidth(width: 10.0)),
                          decoration: BoxDecoration(
                              color: Color(0xff797979),
                              shape: BoxShape.circle,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 2,
                                  blurRadius: 7,
                                  offset: Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ]),
                          child: Icon(
                            Icons.home,
                            color: Colors.white,
                          ),
                        )
                      : Icon(Icons.home)),
              BottomNavigationBarItem(
                  title: Text(''),
                  icon: (bottomNavBarIndex == 1)
                      ? Container(
                          padding:
                              EdgeInsets.all(UIUtills().getWidth(width: 10.0)),
                          decoration: BoxDecoration(
                              color: Color(0xff797979),
                              shape: BoxShape.circle,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 2,
                                  blurRadius: 7,
                                  offset: Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ]),
                          child: Icon(
                            Icons.search,
                            color: Colors.white,
                          ),
                        )
                      : Icon(Icons.search)),
              BottomNavigationBarItem(
                  title: Text(''),
                  icon: (bottomNavBarIndex == 2)
                      ? Container(
                          padding:
                              EdgeInsets.all(UIUtills().getWidth(width: 10.0)),
                          decoration: BoxDecoration(
                              color: Color(0xff797979),
                              shape: BoxShape.circle,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 2,
                                  blurRadius: 7,
                                  offset: Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ]),
                          child: Icon(
                            MdiIcons.briefcaseOutline,
                            color: Colors.white,
                          ),
                        )
                      : Icon(MdiIcons.briefcaseOutline)),
              BottomNavigationBarItem(
                  title: Text(''),
                  icon: (bottomNavBarIndex == 3)
                      ? Container(
                          padding:
                              EdgeInsets.all(UIUtills().getWidth(width: 10.0)),
                          decoration: BoxDecoration(
                              color: Color(0xff797979),
                              shape: BoxShape.circle,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 2,
                                  blurRadius: 7,
                                  offset: Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ]),
                          child: Icon(
                            Icons.favorite_border,
                            color: Colors.white,
                          ),
                        )
                      : Icon(Icons.favorite_border)),
            ],
          ),
        ),
      );
}
