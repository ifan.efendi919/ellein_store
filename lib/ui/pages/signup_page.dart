part of 'pages.dart';

class SignUpPage extends StatefulWidget {
  final RegistrationData registrationData;

  SignUpPage(this.registrationData);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  TextEditingController nameField = TextEditingController();
  TextEditingController emailField = TextEditingController();
  TextEditingController passwordField = TextEditingController();

  bool isEmailValid = false;
  bool isPasswordValid = false;
  bool isSignUp = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            color: mainColor,
          ),
          SafeArea(bottom: false, child: Container(color: accentColor1)),
          ListView(
            shrinkWrap: true,
            physics: ClampingScrollPhysics(),
            children: <Widget>[
              Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  child: IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      context.bloc<PageBloc>().add(GoToSplashPage());
                    },
                  ),
                ),
              ),
              SizedBox(
                height: UIUtills().getHeight(height: 100.0),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: UIUtills().getWidth(width: defaultMargin)),
                child: Column(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () async {
                        if (widget.registrationData.profileImage == null) {
                          widget.registrationData.profileImage = await getImage();
                        } else {
                          widget.registrationData.profileImage = null;
                        }

                        setState(() {});
                      },
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: (widget.registrationData.profileImage == null)
                            ? Container(
                          height: UIUtills().getWidth(width: 100.0),
                          width: UIUtills().getWidth(width: 100.0),
                          margin: EdgeInsets.symmetric(
                              horizontal: UIUtills().getWidth(width: 120.0)),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(color: Colors.grey)),
                          child: Icon(Icons.add_a_photo),
                        )
                            : Container(
                          height: UIUtills().getWidth(width: 100),
                          width: UIUtills().getWidth(width: 100.0),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                  image: FileImage(
                                    widget.registrationData.profileImage,
                                  ),
                                  fit: BoxFit.cover)),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: UIUtills().getHeight(height: 50.0),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Full Name',
                        style: whiteTextFont.copyWith(
                            fontSize: UIUtills().sizeFont(fontsize: 16),
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    SizedBox(
                      height: UIUtills().getHeight(height: 10),
                    ),
                    TextField(
                      controller: nameField,
                      autofocus: true,
                      cursorColor: Color(0xff333333),
                      decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.person_outline,
                          color: accentColor1,
                        ),
                        filled: true,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        hoverColor: Color(0xffEBEBEB),
                        fillColor: Color(0xffEBEBEB),
                        focusColor: Color(0xffEBEBEB),
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: UIUtills().getWidth(width: 15.0)),
                        hintText: 'Full Name',
                        hintStyle: blackTextFont.copyWith(
                          fontSize: UIUtills().sizeFont(fontsize: 14),
                          fontWeight: FontWeight.w400,
                        ),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide.none),
                        disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide.none),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide.none),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Email',
                        style: whiteTextFont.copyWith(
                            fontSize: UIUtills().sizeFont(fontsize: 16),
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    SizedBox(
                      height: UIUtills().getHeight(height: 10),
                    ),
                    TextField(
                      controller: emailField,
                      cursorColor: Color(0xff333333),
                      decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.alternate_email,
                          color: accentColor1,
                        ),
                        filled: true,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        hoverColor: Color(0xffEBEBEB),
                        fillColor: Color(0xffEBEBEB),
                        focusColor: Color(0xffEBEBEB),
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: UIUtills().getWidth(width: 15.0)),
                        hintText: 'Email',
                        hintStyle: blackTextFont.copyWith(
                          fontSize: UIUtills().sizeFont(fontsize: 14),
                          fontWeight: FontWeight.w400,
                        ),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide.none),
                        disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide.none),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide.none),
                      ),
                      onChanged: (text) {
                        setState(() {
                          isEmailValid = EmailValidator.validate(text);
                        });
                      },
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Password',
                        style: whiteTextFont.copyWith(
                            fontSize: UIUtills().sizeFont(fontsize: 16),
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    SizedBox(
                      height: UIUtills().getHeight(height: 10),
                    ),
                    TextField(
                      obscureText: true,
                      controller: passwordField,
                      cursorColor: Color(0xff333333),
                      decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.lock_open,
                          color: accentColor1,
                        ),
                        filled: true,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        hoverColor: Color(0xffEBEBEB),
                        fillColor: Color(0xffEBEBEB),
                        focusColor: Color(0xffEBEBEB),
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: UIUtills().getWidth(width: 15.0)),
                        hintText: 'Password',
                        hintStyle: blackTextFont.copyWith(
                          fontSize: UIUtills().sizeFont(fontsize: 14),
                          fontWeight: FontWeight.w400,
                        ),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide.none),
                        disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide.none),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide.none),
                      ),
                      onChanged: (text) {
                        setState(() {
                          isPasswordValid = text.length >= 6;
                        });
                      },
                    ),
                    SizedBox(height: UIUtills().getHeight(height: 20.0)),
                    (isSignUp)
                        ? SpinKitFadingCircle(
                      color: accentColor2,
                    )
                        : RaisedButton(
                      onPressed: () async {
                        setState(() {
                          isSignUp = true;
                        });

                        imageFileToUpload =
                            widget.registrationData.profileImage;

                        SignInSignUpResult result =
                        await AuthServices.signUp(
                            nameField.text,
                            emailField.text,
                            passwordField.text);

                        if (result.user == null) {
                          setState(() {
                            isSignUp = false;
                          });

                          _showBasicsFlash(message: result.message);
                        }else{
                          setState(() {
                            isSignUp = false;
                          });
                          context.bloc<PageBloc>().add(GoToMainPage());
                        }
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      padding: const EdgeInsets.all(0.0),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.only(
                          top: UIUtills().getHeight(height: 15.0),
                          bottom: UIUtills().getHeight(height: 15.0),
                        ),
                        decoration: BoxDecoration(
                            color: Colors.grey,
                            borderRadius: BorderRadius.circular(10)),
                        child: Text(
                          'Register Now',
                          style: whiteTextFont.copyWith(
                              fontSize: UIUtills().sizeFont(fontsize: 16),
                              fontWeight: FontWeight.w600),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    SizedBox(height: UIUtills().getHeight(height: 20.0)),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Row(
                        children: <Widget>[
                          Text(
                            'Already have an account ? ',
                            style: whiteTextFont.copyWith(
                                fontSize: UIUtills().sizeFont(fontsize: 14),
                                fontWeight: FontWeight.w600),),
                          GestureDetector(
                            onTap: () async {},
                            child: Text(
                              'Login',
                              style: whiteTextFont.copyWith(
                                  fontSize: UIUtills().sizeFont(fontsize: 14),
                                  fontWeight: FontWeight.w600, color: Colors.deepOrange),),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          )
        ],
      )
    );
  }

  void _showBasicsFlash(
      {Duration duration, flashStyle = FlashStyle.floating, String message}) {
    showFlash(
      context: context,
      duration: Duration(seconds: 2),
      builder: (context, controller) {
        return Flash(
          controller: controller,
          backgroundColor: Colors.redAccent,
          style: flashStyle,
          boxShadows: kElevationToShadow[1],
          horizontalDismissDirection: HorizontalDismissDirection.horizontal,
          position: FlashPosition.top,
          barrierBlur: 3.0,
          barrierColor: Colors.black38,
          barrierDismissible: true,
          child: FlashBar(
            message: Text(
              '$message',
              style: whiteTextFont,
            ),
          ),
        );
      },
    );
  }
}
