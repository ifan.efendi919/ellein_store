
import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flash/flash.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:store/bloc/blocs.dart';
import 'package:store/models/models.dart';
import 'package:store/services/services.dart';
import 'package:store/shared/shared.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:store/ui/widgets/widgets.dart';

part 'wrapper.dart';
part 'login_page.dart';
part 'signup_page.dart';
part 'main_page.dart';
part 'product_page.dart';
part 'product_create_page.dart';