part of 'pages.dart';

class ProductCreatePage extends StatefulWidget {
  final CreateProductData createProductData;
  ProductCreatePage(this.createProductData);

  @override
  _ProductCreatePageState createState() => _ProductCreatePageState();
}

class _ProductCreatePageState extends State<ProductCreatePage> {
  TextEditingController nameField = TextEditingController();
  TextEditingController descriptionField = TextEditingController();
  TextEditingController priceField = TextEditingController();
  TextEditingController colorField = TextEditingController();
  bool checkedM;
  bool checkedL;
  bool checkedXL;
  bool isCreated = false;

  List<String> listSize = List();
  List<String> listColor = List();

  @override
  void initState() {
    super.initState();

    checkedM = false;
    checkedL = false;
    checkedXL = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: accentColor1,
          ),
          onPressed: () {
            context.bloc<PageBloc>().add(GoToMainPage());
          },
        ),
        title: Text(
          'Create Product',
          style: blackTextFont.copyWith(
              fontSize: UIUtills().sizeFont(fontsize: 16),
              fontWeight: FontWeight.bold),
        ),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(
            horizontal: UIUtills().getWidth(width: defaultMargin)),
        shrinkWrap: true,
        children: <Widget>[
          SizedBox(
            height: UIUtills().getHeight(height: 20.0),
          ),
          GestureDetector(
            onTap: () async {
              if (imageProductToUpload == null) {
                imageProductToUpload = await getImage();
              } else {
                imageProductToUpload = null;
              }

              setState(() {});
            },
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: (imageProductToUpload == null)
                  ? Container(
                      height: UIUtills().getWidth(width: 100.0),
                      width: UIUtills().getWidth(width: 100.0),
                      margin: EdgeInsets.symmetric(
                          horizontal: UIUtills().getWidth(width: 120.0)),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: Colors.grey)),
                      child: Icon(Icons.add_a_photo),
                    )
                  : Container(
                      height: UIUtills().getWidth(width: 200),
                      margin: EdgeInsets.symmetric(
                          horizontal: UIUtills().getWidth(width: 120.0)),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          image: DecorationImage(
                              image: FileImage(
                                imageProductToUpload,
                              ),
                              fit: BoxFit.cover)),
                    ),
            ),
          ),
          SizedBox(
            height: UIUtills().getHeight(height: 10.0),
          ),
          TextField(
            controller: nameField,
            autofocus: true,
            cursorColor: Color(0xff333333),
            decoration: InputDecoration(
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: accentColor1)),
              hoverColor: Color(0xffEBEBEB),
              fillColor: Color(0xffEBEBEB),
              focusColor: Color(0xffEBEBEB),
              contentPadding: EdgeInsets.symmetric(
                  horizontal: UIUtills().getWidth(width: 15.0)),
              hintText: 'Name Product',
              hintStyle: blackTextFont.copyWith(
                fontSize: UIUtills().sizeFont(fontsize: 12),
                fontWeight: FontWeight.w400,
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              disabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
          SizedBox(
            height: UIUtills().getHeight(height: 10.0),
          ),
          TextField(
            controller: descriptionField,
            autofocus: false,
            cursorColor: Color(0xff333333),
            decoration: InputDecoration(
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: accentColor1)),
              hoverColor: Color(0xffEBEBEB),
              fillColor: Color(0xffEBEBEB),
              focusColor: Color(0xffEBEBEB),
              contentPadding: EdgeInsets.symmetric(
                  horizontal: UIUtills().getWidth(width: 15.0)),
              hintText: 'Description Product',
              hintStyle: blackTextFont.copyWith(
                fontSize: UIUtills().sizeFont(fontsize: 12),
                fontWeight: FontWeight.w400,
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              disabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
          SizedBox(
            height: UIUtills().getHeight(height: 10.0),
          ),
          TextField(
            controller: priceField,
            autofocus: false,
            cursorColor: Color(0xff333333),
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: accentColor1)),
              hoverColor: Color(0xffEBEBEB),
              fillColor: Color(0xffEBEBEB),
              focusColor: Color(0xffEBEBEB),
              contentPadding: EdgeInsets.symmetric(
                  horizontal: UIUtills().getWidth(width: 15.0)),
              hintText: 'Price Product',
              hintStyle: blackTextFont.copyWith(
                fontSize: UIUtills().sizeFont(fontsize: 12),
                fontWeight: FontWeight.w400,
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              disabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
          SizedBox(
            height: UIUtills().getHeight(height: 10.0),
          ),
          Container(
            height: UIUtills().getHeight(height: 40.0),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: CheckboxListTile(
                    title: Text("M"),
                    value: checkedM,
                    onChanged: (val) {
                      setState(() {
                        checkedM = !checkedM;
                        if (checkedM) {
                          listSize.add('M');
                        } else {
                          listSize.remove('M');
                        }
                      });
                    },
                    controlAffinity: ListTileControlAffinity
                        .leading, //  <-- leading Checkbox
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: CheckboxListTile(
                    title: Text("L"),
                    value: checkedL,
                    onChanged: (val) {
                      setState(() {
                        checkedL = !checkedL;
                        if (checkedL) {
                          listSize.add('L');
                        } else {
                          listSize.remove('L');
                        }
                      });
                    },
                    controlAffinity: ListTileControlAffinity
                        .leading, //  <-- leading Checkbox
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: CheckboxListTile(
                    title: Text("XL"),
                    value: checkedXL,
                    onChanged: (val) {
                      setState(() {
                        checkedXL = !checkedXL;
                        if (checkedXL) {
                          listSize.add('XL');
                        } else {
                          listSize.remove('XL');
                        }
                      });
                    },
                    controlAffinity: ListTileControlAffinity
                        .leading, //  <-- leading Checkbox
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: UIUtills().getHeight(height: 20.0),
          ),
          InputTags(
            color: Colors.grey,
            backgroundContainer: Colors.transparent,
            borderRadius: 10,
            offset: 50,
            columns: 5,
            autofocus: false,
            alignment: MainAxisAlignment.start,
            fontSize: UIUtills().sizeFont(fontsize: 14),
            inputDecoration: InputDecoration(
              hintText: "Add Colors",
              hintStyle: blackTextFont.copyWith(
                  fontSize: UIUtills().sizeFont(fontsize: 14)),
              fillColor: Colors.transparent,
              border: new OutlineInputBorder(
                  borderSide: new BorderSide(),
                  borderRadius: BorderRadius.circular(10)),
              contentPadding: EdgeInsets.all(10),
            ),
            tags: [],
            onDelete: (tag) {
              listColor.remove(tag.toUpperCase());
            },
            onInsert: (tag) {
              listColor.add(tag.toUpperCase());
            },
          ),
          SizedBox(
            height: UIUtills().getHeight(height: 15.0),
          ),
          (isCreated)
              ? SpinKitFadingCircle(
                  color: accentColor2,
                )
              : Container(
                  margin: EdgeInsets.symmetric(
                      horizontal: UIUtills().getWidth(width: 100.0)),
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    onPressed: () async {
                      if (!(nameField.text.trim() != "" &&
                          descriptionField.text.trim() != "" &&
                          priceField.text.trim() != "")) {
                        _showBasicsFlash(message: 'Please fill all the fields');
                      } else {
                        setState(() {
                          isCreated = true;
                        });
                        CreateProductData transaction;

                        if (imageProductToUpload != null) {
                          uploadImage(imageProductToUpload).then((downloadURL) async {
                            widget.createProductData.imageProduct = null;
                            transaction = CreateProductData(
                                nameProduct: nameField.text,
                                descriptionProduct: descriptionField.text,
                                priceProduct: int.parse(priceField.text),
                                sizeProduct: listSize,
                                colorProduct: listColor,
                                imageProduct: downloadURL);

                            await ProductServices.saveTransaction(transaction);
                            setState(() {
                              isCreated = false;
                            });

                            context.bloc<PageBloc>().add(GoToMainPage());
                          });
                        }
                      }
                    },
                    padding: const EdgeInsets.all(0.0),
                    child: Container(
                      height: UIUtills().getHeight(height: 35.0),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: mainColor,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Center(
                        child: Text(
                          'Save',
                          style: whiteTextFont.copyWith(
                              fontSize: UIUtills().sizeFont(fontsize: 16),
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                )
        ],
      ),
    );
  }

  void _showBasicsFlash(
      {Duration duration, flashStyle = FlashStyle.floating, String message}) {
    showFlash(
      context: context,
      duration: Duration(seconds: 2),
      builder: (context, controller) {
        return Flash(
          controller: controller,
          backgroundColor: Colors.redAccent,
          style: flashStyle,
          boxShadows: kElevationToShadow[1],
          horizontalDismissDirection: HorizontalDismissDirection.horizontal,
          position: FlashPosition.top,
          barrierBlur: 3.0,
          barrierColor: Colors.black38,
          barrierDismissible: true,
          child: FlashBar(
            message: Text(
              '$message',
              style: whiteTextFont,
            ),
          ),
        );
      },
    );
  }
}
