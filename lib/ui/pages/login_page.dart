part of 'pages.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController emailField = TextEditingController();
  TextEditingController passwordField = TextEditingController();

  bool isEmailValid;
  bool isPasswordValid;
  bool isSigningIn;

  @override
  void initState() {
    super.initState();

    isEmailValid = false;
    isPasswordValid = false;
    isSigningIn = false;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            color: mainColor,
          ),
          SafeArea(bottom: false, child: Container(color: accentColor1)),
          ListView(
            shrinkWrap: true,
            physics: ClampingScrollPhysics(),
            children: <Widget>[
              Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  child: IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      context.bloc<PageBloc>().add(GoToMainPage());
                    },
                  ),
                ),
              ),
              SizedBox(
                height: UIUtills().getHeight(height: 100.0),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: UIUtills().getWidth(width: defaultMargin)),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'ELLEIN STORE',
                      style: whiteTextFont.copyWith(
                          fontSize: UIUtills().sizeFont(fontsize: 30),
                          fontWeight: FontWeight.w600),
                    ),
                    SizedBox(
                      height: UIUtills().getHeight(height: 50.0),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Email',
                        style: whiteTextFont.copyWith(
                            fontSize: UIUtills().sizeFont(fontsize: 16),
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    SizedBox(
                      height: UIUtills().getHeight(height: 10),
                    ),
                    TextField(
                      controller: emailField,
                      autofocus: true,
                      cursorColor: Color(0xff333333),
                      decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.alternate_email,
                          color: accentColor1,
                        ),
                        filled: true,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        hoverColor: Color(0xffEBEBEB),
                        fillColor: Color(0xffEBEBEB),
                        focusColor: Color(0xffEBEBEB),
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: UIUtills().getWidth(width: 15.0)),
                        hintText: 'Email',
                        hintStyle: blackTextFont.copyWith(
                          fontSize: UIUtills().sizeFont(fontsize: 14),
                          fontWeight: FontWeight.w400,
                        ),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide.none),
                        disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide.none),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide.none),
                      ),
                      onChanged: (text) {
                        setState(() {
                          isEmailValid = EmailValidator.validate(text);
                        });
                      },
                    ),
                    SizedBox(
                      height: UIUtills().getHeight(height: 15.0),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Password',
                        style: whiteTextFont.copyWith(
                            fontSize: UIUtills().sizeFont(fontsize: 16),
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    SizedBox(
                      height: UIUtills().getHeight(height: 10),
                    ),
                    TextField(
                      controller: passwordField,
                      obscureText: true,
                      autofocus: false,
                      cursorColor: Color(0xff333333),
                      decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.lock_open,
                          color: accentColor1,
                        ),
                        filled: true,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        hoverColor: Color(0xffEBEBEB),
                        fillColor: Color(0xffEBEBEB),
                        focusColor: Color(0xffEBEBEB),
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: UIUtills().getWidth(width: 15.0)),
                        hintText: 'Password',
                        hintStyle: blackTextFont.copyWith(
                          fontSize: UIUtills().sizeFont(fontsize: 14),
                          fontWeight: FontWeight.w400,
                        ),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide.none),
                        disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide.none),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide.none),
                      ),
                      onChanged: (text) {
                        setState(() {
                          isPasswordValid = text.length >= 6;
                        });
                      },
                    ),
                    SizedBox(height: UIUtills().getHeight(height: 20.0)),
                    (isSigningIn)
                        ? SpinKitFadingCircle(
                            color: accentColor2,
                          )
                        : RaisedButton(
                            onPressed: () async {
                              setState(() {
                                isSigningIn = true;
                              });

                              SignInSignUpResult result =
                                  await AuthServices.signIn(
                                      emailField.text, passwordField.text);

                              if (result.user == null) {
                                setState(() {
                                  isSigningIn = false;
                                });

                                _showBasicsFlash(message: result.message);
                              }else{
                                setState(() {
                                  isSigningIn = false;
                                });
                                context.bloc<PageBloc>().add(GoToMainPage());
                              }
                            },
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            padding: const EdgeInsets.all(0.0),
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(
                                top: UIUtills().getHeight(height: 15.0),
                                bottom: UIUtills().getHeight(height: 15.0),
                              ),
                              decoration: BoxDecoration(
                                  color: Colors.grey,
                                  borderRadius: BorderRadius.circular(10)),
                              child: Text(
                                'LOGIN',
                                style: whiteTextFont.copyWith(
                                    fontSize: UIUtills().sizeFont(fontsize: 16),
                                    fontWeight: FontWeight.w600),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                    SizedBox(height: UIUtills().getHeight(height: 20.0)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          'Don\'t have account? ',
                          style: whiteTextFont.copyWith(
                              fontSize: UIUtills().sizeFont(fontsize: 14),
                              fontWeight: FontWeight.w600),),
                        GestureDetector(
                          onTap: (){
                            context.bloc<PageBloc>().add(GoToRegistrationPage(RegistrationData()));
                          },
                          child: Text(
                            'Register',
                            style: whiteTextFont.copyWith(
                                fontSize: UIUtills().sizeFont(fontsize: 14),
                                fontWeight: FontWeight.w600, color: Colors.deepOrange),),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  void _showBasicsFlash(
      {Duration duration, flashStyle = FlashStyle.floating, String message}) {
    showFlash(
      context: context,
      duration: Duration(seconds: 2),
      builder: (context, controller) {
        return Flash(
          controller: controller,
          backgroundColor: Colors.redAccent,
          style: flashStyle,
          boxShadows: kElevationToShadow[1],
          horizontalDismissDirection: HorizontalDismissDirection.horizontal,
          position: FlashPosition.top,
          barrierBlur: 3.0,
          barrierColor: Colors.black38,
          barrierDismissible: true,
          child: FlashBar(
            message: Text(
              '$message',
              style: whiteTextFont,
            ),
          ),
        );
      },
    );
  }
}
